import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    public static void main(String args[])
    {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        JPanel panel = new JPanel();
        JButton btn1 = new JButton("Press to display text in console");
        JTextField textField = new JTextField(20);
        textField.setText("Hello");

        panel.add(btn1);
        panel.add(textField);

        panel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.setPreferredSize(new Dimension(400, 100));
        panel.setMaximumSize(new Dimension(400, 100));
        frame.getContentPane().add(panel);
        frame.setSize(550, 300);
        frame.setVisible(true);

        btn1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String finalText = new StringBuffer(textField.getText()).reverse().toString();
                System.out.println(finalText);
            }
        });
    }
}
